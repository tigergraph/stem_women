## Steps to get started

1. Install the requirements.txt
2. run the commands "python manage.py makemigrations" and "python manage.py migrate" to make the migrations
3. Uncomment the function calls in /authentication/views.py to populate the data in the database.
4. Comment the function calls after the intial run to avoid the duplication.
5. Run the server using "python manage.py runserver" to run the application.

