from enum import unique
import random

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User,auth
from django.contrib import messages
from django.shortcuts import render,redirect
from django.contrib.auth import login, logout , authenticate
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth.decorators import login_required
from .models import  CustomUser,Connections, Jobs, Courses, Opportunities
import pandas as pd
import datetime
import ast
import pyTigerGraph as tg
from nltk import word_tokenize, sent_tokenize
from nltk.stem.porter import PorterStemmer
porter = PorterStemmer()
from nltk.corpus import stopwords

# from .utils import user_Required
from app_cashing import Cache
from django.db.models import Q

current_timestamp = datetime.datetime.now()

# hostName = "http://10.100.252.136"
# graphName = "stem_women"
# secret = "15rlr76gf2iihv2q4dk7ihjsjc13m1fr"
# username = "tigergraph"
# password = "tigergraph"
# graph = tg.TigerGraphConnection(host=hostName,password=password, graphname=graphName)

hostName = "https://110797768b66481c98de7b99a48c777a.i.tgcloud.io/"
graphName = "stem_women"
secret = "v19agbbg5srahq1k73mclke7aend0rf6"
username = "tigergraph"
password = "123Capgemini"
conn = tg.TigerGraphConnection(host=hostName,password=password, graphname=graphName)

authToken = conn.getToken(secret)
authToken = authToken[0]
graph = tg.TigerGraphConnection(host=hostName, graphname=graphName, username=username, password=password,
                               apiToken=authToken)


# Create your views here.
def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            print("user Created")
            return redirect('/')
    form = UserCreationForm()
    return render(request, 'bs4_sign_up_page.html',{'form': form})

def login_user(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                print('user logged in')
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("/")
            else:
                messages.error(request,"Invalid username or password.")
        else:
            messages.error(request,"Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request, template_name="Dark_Login.html", context={"form":form})

@login_required
def logout_request(request):
    logout(request)
    messages.info(request, "You have successfully logged out.")
    return redirect("login")

@login_required
def HomePage(request):
    
    current_user = request.user
    user_id = current_user.id
    unique_id = current_user.unique_id

    user_details = CustomUser.objects.filter(id=user_id).values()[:1].get()


    if request.method=='POST':
        leaderid = request.POST.get('leaderid')
        score = request.POST.get('score')
     
        # edge_query = "CREATE QUERY insertEdge() FOR GRAPH stem_women { " + "INSERT INTO connected VALUES (6 employee, {} leader, {});".format(leaderid, score) +"}"
        # query = graph.gsql( edge_query)
        # graph.gsql("INSTALL QUERY insertEdge")

        parameters = {'e':unique_id, 'l':leaderid, 'score':score}
        run_query = graph.runInstalledQuery('insertEdge', params=parameters, timeout=1000000)
        graph.parseQueryOutput(run_query)

        parameters_recommend = {'p':int(unique_id), 'k1':50, 'k2':20}
        query = graph.runInstalledQuery('recommend', params=parameters_recommend, timeout=1000000)
        parsed = graph.parseQueryOutput(query) 
        print(parsed)

        results = ((parsed['vertices']['leader']))
        
        
        data = {"user_id":user_details["unique_id"],"username":user_details["username"],
            "email":user_details["email"],"Name":user_details["name"], "designation":user_details["designation"], 
            "data":results}

        return render(request, 'LandingPage.html') 

    else:
       
        parameters = {'p':int(unique_id), 'k1':50, 'k2':20}
        query = graph.runInstalledQuery('recommend', params=parameters, timeout=1000000)
        parsed = graph.parseQueryOutput(query)  
        results = ((parsed['vertices']['leader']))
        data = {"user_id":user_details["unique_id"],"username":user_details["username"],
            "email":user_details["email"],"Name":user_details["name"], "designation":user_details["designation"]
           , "data":results}

        # print(data["data"])

        return render(request, 'LandingPage.html',data)

@login_required
def editPage(request):
    current_user = request.user
    user_id = current_user.id
    username = current_user.username
    email = current_user.email
    Name = current_user.get_full_name()
    data = {"user_id":user_id,"username":username,"email":email,"Name":Name}
    return render(request, 'account_setting_or_edit_profile.html',data)

@login_required
def connections_page(request):
    current_user = request.user
    unique_id = current_user.unique_id
    # print(unique_id)
    Name = current_user.name
    designation = current_user.designation
    parameters = {'p':unique_id}
    query = graph.runInstalledQuery('get_connections', params=parameters, timeout=100000)
    parsed = graph.parseQueryOutput(query)
    print(parsed)
    connections = parsed['vertices']['leader'] 
    data = {"name":Name, "designation":designation, "connection":connections, "count":len(connections)}
    return render(request, 'connections.html', data)

@login_required
def About_me_page(request):
    current_user = request.user
    user_id = current_user.id
    username = current_user.username
    email = current_user.email
    Name = current_user.name
    phone = current_user.phone
    skills = current_user.skills
    slill_list = list(ast.literal_eval(skills))
    rand_int = [50,60,80,90]
    slill_list = [{"skill":i,"score":random.choice(rand_int)} for i in slill_list]
    # slill_list = []

    data = {"user_id":user_id,"username":username,"email":email,"Name":Name,"phone":phone,"skills_list":slill_list}

    # print(data)

    return render(request, 'about_me_profile.html',data)

@login_required
def job_post(request):
    current_user = request.user
    user_id = current_user.id
    user_details = CustomUser.objects.filter(id=user_id).values()[:1].get()
    location = user_details["location"]
    jobs = Opportunities.objects.filter(Location__contains=location).values()[:10]
    data = []
    for i in jobs.iterator():
       data.append(i)
    return render(request, 'General_Search_Results.html',{"data":data})

data = []
def get_match_score(text_a,course):
    punc = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    slills_str = course["skills"]
    skills_set = set(slills_str.split(" "))
    text_b = sum([word_tokenize(i) for i in skills_set], [])
    text_b = [word for word in text_b if not word in stopwords.words()]
    text_b = [word for word in text_b if not word in punc]
    text_b = set([porter.stem(item) for item in text_b])

    match = (len(text_a.intersection(text_b))) / maximum(len(text_a), len(text_b)) * 100
    # print("match :", match)
    if match > 20:
        data.append(course)
import threading
def process_requests(text_a,Courses):
    all_threads = []
    for course in Courses:
        thread = threading.Thread(target=get_match_score, args=(text_a,course))
        thread.start()
        all_threads.append(thread)
    for thread in all_threads:
        thread.join()

    return data

@login_required
def get_courses(request):
    courses = []
    current_user = request.user
    unique_id = current_user.unique_id
    user_id = current_user.id
    if Cache.get(str(user_id) + str(unique_id)):
        return render(request, 'bs5_forum_list.html', {"courses":Cache.get(str(user_id) + str(unique_id))})
    parameters = {'p':unique_id}
    query = graph.runInstalledQuery('get_connections', params=parameters, timeout=100000)
    parsed = graph.parseQueryOutput(query)
    connections = parsed['vertices']['leader']
    for i,j in connections.items():
        leader_id = i
        param = {'l':leader_id, 'k1':2, 'k2':2}
        query = graph.runInstalledQuery('get_courses', params=param, timeout=100000)
        parsed = graph.parseQueryOutput(query)
        
        if len(parsed['vertices']) > 0:
            courses_dict = parsed['vertices']['certifications']
            courses.append(courses_dict)
        else:
            pass
    Cache.put(str(user_id) + str(unique_id), courses)
    return render(request, 'bs5_forum_list.html', {"courses":courses})


def insert_to_connections():
    df = pd.read_csv('authentication/data/Connections_migration_data.csv')
    print(df)
    for index, rows in df.iterrows():
        c = Connections(leader_id=rows['leader'],employee_id=rows['employee'],score=rows['match_score'],date=current_timestamp)
        c.save()

def insert_to_users():
    df = pd.read_csv('authentication/data/Users_migration_data.csv')
    for index, rows in df.iterrows():
        w = CustomUser(name=rows['name'],
                       unique_id=rows['unique_id'],
                       username=rows['name'].replace(" ","")+str(rows['unique_id']),
                       email=rows['email'],
                       password=rows['password'],
                       location=rows['location'],
                       designation=rows['designation'],
                       skills=rows['skills'],
                       experience=rows['exp_in_years'],
                       role=rows['role'],
                       last_login=current_timestamp)
        w.save()
        
def insert_to_jobs():
    df = pd.read_csv('authentication/data/job_data.csv')
    print(df)
    for index, rows in df.iterrows():
        w = Opportunities(job_title=rows['Job Title'],description=rows['Job Description'],
                 skills=rows['Skills'],companyname=rows['Company'],
                 Location=rows['Location'],salary=rows['Job Salary'],date=current_timestamp)
        w.save()

def insert_to_courses():
    df = pd.read_csv('authentication/data/certification_data.csv')
    for index, rows in df.iterrows():
        w = Courses(name=rows['Course Name'],
                       university=rows['University'],
                       difficulty_level=rows['Difficulty Level'],
                       course_ratings=rows['Course Rating'],
                       course_url=rows['Course URL'],
                       description=rows['Course Description'],
                       skills=rows['Skills'],
                       publish_date= current_timestamp)
        w.save()

# insert_to_users()
# insert_to_connections()
# insert_to_jobs()
# insert_to_courses()