from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime

current_timestamp = datetime.datetime.now()

# Create your models here.
class CustomUser(AbstractUser):
    unique_id = models.IntegerField(null=True)
    name = models.CharField(max_length=250,null=True)
    role = models.CharField(max_length=250,null=True)
    experience = models.TextField(max_length=250)
    skills = models.TextField(null=True)
    designation = models.TextField(null=True)
    location = models.TextField(null=True)
    phone = models.TextField(null=True)
    photo = models.TextField(null=True)


    class Meta:
        ordering = ['name']

class Connections(models.Model):
    # id = models.IntegerField(primary_key=True)
    leader_id = models.IntegerField()
    employee_id = models.IntegerField()
    score = models.FloatField()
    date = models.DateTimeField()

class Jobs(models.Model):
    # id = models.IntegerField(primary_key=True)
    job_title = models.TextField(default=None)
    description = models.TextField()
    skills = models.TextField()
    companyname = models.TextField(default=None)
    Location = models.TextField()
    salary = models.TextField()
    date = models.DateTimeField()

class Courses(models.Model):
    # id = models.IntegerField(primary_key=True)
    name = models.TextField(default=None)
    university = models.TextField()
    difficulty_level = models.TextField()
    course_ratings = models.TextField(default=None)
    course_url = models.TextField()
    description = models.TextField()
    skills = models.DateTimeField()
    publish_date = models.DateTimeField()

class Opportunities(models.Model):
    # id = models.IntegerField(primary_key=True)
    job_title = models.TextField(default=None)
    description = models.TextField()
    skills = models.TextField()
    companyname = models.TextField(default=None)
    Location = models.TextField()
    salary = models.TextField()
    date = models.DateTimeField()