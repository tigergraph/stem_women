from django.urls import path, include
from . import views

urlpatterns = [
    path('register', views.register, name="register"),
    path('login', views.login_user, name="login"),
    path('', views.HomePage, name="homepage"),
    path("logout", views.logout_request, name="logout"),
    path("edit", views.editPage, name="editPage"),
    path("connections", views.connections_page, name="connections_page"),
    path("profile-page", views.About_me_page, name="About_me_page"),
    path("job-post", views.job_post, name="job_post"),
    path("courses", views.get_courses, name="courses"),
]