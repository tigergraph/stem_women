import pandas as pd
import random
import numpy as np

df = pd.read_csv("data/Users_migration_data.csv")

# print(df.head())

# df["location"] = random.choice(['kolkata','chennai','mumbai','delhi'])
df["location"] = np.random.choice(['kolkata','chennai','mumbai','delhi'], df.shape[0])

print(df.to_csv('data/Users_migration_data.csv'))